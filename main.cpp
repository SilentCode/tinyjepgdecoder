
#include <stdio.h>
#include "JpegDecoder.h"

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Usage: main <parameters> \n");
        return 0;
    }

	JpegDecoder decoder(argv[1]);
    BitmapImage &img = decoder.Decoder();

    // 使用 OpenCV 显示图像
    Mat bitmap;
    bitmap.create(img.Height, img.Width, CV_8U3C); // CV_8U3C = 24位真彩色
    bitmap.data = img.Data;
    imshow("Bitmap", bitmap);

	return 0;
}
